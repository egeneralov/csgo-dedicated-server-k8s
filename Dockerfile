ARG RELEASE=bullseye
FROM debian:${RELEASE}
ARG RELEASE=bullseye
RUN echo "deb http://deb.debian.org/debian ${RELEASE} main contrib non-free" > /etc/apt/sources.list
RUN \
  echo steam steam/question select "I AGREE" | debconf-set-selections && \
  useradd -m steam && \
  dpkg --add-architecture i386 && \
  apt-get update -q && \
  apt-get install -yq curl lib32gcc-s1
USER steam
WORKDIR /home/steam
RUN \
  curl -sL http://media.steampowered.com/installer/steamcmd_linux.tar.gz | tar xzvf -
ENV PATH=/home/steam:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
COPY --chown=steam:steam csgo_install.txt /home/steam/
# /root/Steam/logs/stderr.txt
# RUN steamcmd.sh +runscript /home/steam/csgo_install.txt
COPY --chown=steam:steam entry.sh /home/steam/
EXPOSE 27015/tcp
EXPOSE 27015/udp
EXPOSE 27020/udp
EXPOSE 27005/udp
# CMD ["/home/steam/entry.sh"]
# /home/steam/Steam/logs/stderr.txt